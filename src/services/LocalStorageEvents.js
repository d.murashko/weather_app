export default class LocalStorageEvents {
  static getData(dataName) {
    let data = localStorage.getItem(dataName);
    return JSON.parse(data);
  }

  static setData(dataName, data) {
    const stringData = JSON.stringify(data);
    localStorage.setItem(dataName, stringData);
  }

  static clearData() {
    localStorage.clear();
  }
}
