import { createStore, compose } from "redux";
import mainReducer from "../reducers/mainReducer";

const ConfigStore = (initState) => {
    const composeEnhancers =
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;


    return createStore(
        mainReducer,
        initState,
        composeEnhancers()
    );
};

export default ConfigStore;