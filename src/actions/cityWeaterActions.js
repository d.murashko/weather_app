import * as types from "../constants/constTypes";

export function loading() {
    return (dispatch) => {
        dispatch({
            type: types.LOADING
        });
    };
}

export function getCityWeather(data) {
    return (dispatch) => {
        dispatch({
            type: types.ADD_CITY_WEATHER,
            payload: data
        });
    };
}