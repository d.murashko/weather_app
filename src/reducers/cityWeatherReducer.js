import * as types from "../constants/constTypes";

const initState = {
    cityWeather: [],
    loading: false
};

const cityWeatherReducer = (state = initState, action) => {
    switch (action.type) {
        case types.LOADING:
            return {
                ...state,
                ...{loading: true}
            };
        case types.ADD_CITY_WEATHER:
            return {
                cityWeather: action.payload,
                loading: false
            };
        default:
            return state;
    }
};

export default cityWeatherReducer;